#pragma once
#include <DirectXMath.h>
#include <vector>

class Collision
{
    struct  Face
    {
        DirectX::XMFLOAT3 position[3];
        int materialIndex; //マテリアル番号
    };
    std::vector<Face> faces;

public:
    //〇と〇
   static bool  HitCollision(DirectX::XMFLOAT3 ShotPos, float Sradius, DirectX::XMFLOAT3 EPos, float Eradius);


    int RayPick(const DirectX::XMFLOAT3&startPosition,
        const DirectX::XMFLOAT3& endPosition,
        DirectX::XMFLOAT3* outPosition,
        DirectX::XMFLOAT3* outNormal,
        float* outLenght);




};