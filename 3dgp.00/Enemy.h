#pragma once
#include <memory>
#include <vector>
#include "model.h"
#include "model_renderer.h"


//関数ポインタ
typedef void(*MOVER)();



using namespace DirectX;

struct HitSize
{
    XMFLOAT3 size;
};

enum EnemyType
{
    Blue,
    Red,
    NORMAL,
};

static XMFLOAT4 ColorList[] =
{
    XMFLOAT4(0.0f,0.0f,1.0f,1.0f),//青
    XMFLOAT4(1.0f,0.0f,0.0f,1.0f),//赤
    XMFLOAT4(1.0f,1.0f,1.0f,1.0f),//通常
};
//----------------------------------
//  class:C_Enemy
//----------------------------------
class C_Enemy
{
private:
    std::shared_ptr<Model> obj;
    std::shared_ptr<ModelRenderer> obj_Render;

    XMFLOAT3 pos;
    XMFLOAT3 angle;
    XMFLOAT3 scale;
    XMFLOAT4 color;
    //XMFLOAT2 speed;

    bool exist;

    MOVER mover;

    XMFLOAT3 HitVec;//当たった時に進む向き

    EnemyType Type;

    float speed;

    float RefrectTime;//反射する時間
    bool Hit;//反射していればtrue
public:
    C_Enemy();
    void Init(ID3D11Device*device, std::shared_ptr<ModelResource>& resource);
    void Init(ID3D11Device*device);
    void Update(float elapsedTime, XMFLOAT3 pos);
    void Render(ID3D11DeviceContext* context, float elapsed_time, const XMFLOAT4X4 &view, const DirectX::XMFLOAT4&light_direction);
    void Set(XMFLOAT3 pos);
    void Destroy();
    bool isAlive() { return exist; };
    void SetType(EnemyType num)
    {
        Type = num;
        color = ColorList[Type];
    };
    void add(XMFLOAT3 pos, float angle);

    Model GetModel()
    {
        return *obj;
    }

    XMFLOAT3 GetPos()const { return pos; };

    void SetSpeed(float speed)
    {
        this->speed = speed;
    };

    void SetHitVec(XMFLOAT3 Vec)
    {
        this->HitVec = Vec;
    };

    void SetHit(bool hit)
    {
        this->Hit = hit;
    };

    EnemyType GetType() { return Type; };
};

//----------------------------------
//  class:C_EnemyManager
//----------------------------------

struct WindowSize
{
    int MaxX, MinX, MaxZ, MinZ;
};
enum SpawnType
{
    Top,//上
    Down,//下
    Right,//右
    Left,//左
};

enum HitType//磁力の当たり判定
{
    Same,//同じ
    Inverse,//反対
    Normal,//それ以外
};
class C_EnemyManager
{
private:
    const static int EnemyMax = 100;

    //std::vector<C_Enemy> list;

    C_Enemy list[100];

    std::shared_ptr<ModelResource> model_resource;

    WindowSize window;
    C_EnemyManager() {};

    float size;//メッシュのサイズ
    float  magnet;//磁力
private:
    void add(XMFLOAT3 pos);
    void HitMove(C_Enemy*My, C_Enemy*HitModel, HitType hit);

    HitType GetHitType(EnemyType Type1, EnemyType Type2);
public:
    void Init(ID3D11Device*device);
    void Update(float elapsedTime, XMFLOAT3 pos);
    void Render(ID3D11DeviceContext* context, float elapsed_time, const XMFLOAT4X4 &view, const DirectX::XMFLOAT4&light_direction);

    
    C_Enemy* GetInfo(int num)
    {
        return &list[num];
    };

    static C_EnemyManager *getInstance()
    {
        static C_EnemyManager instance;
        return &instance;
    }


};

#define pEnemyManager C_EnemyManager::getInstance()
