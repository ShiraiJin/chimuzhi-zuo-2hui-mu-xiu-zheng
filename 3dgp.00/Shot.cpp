#include "Shot.h"
#include "fbx_load.h"

Shot::Shot(ID3D11Device* device) {
    pos = DirectX::XMFLOAT3(0, 0, 0);
    angle = DirectX::XMFLOAT3(0, 0, 0);
    scale = DirectX::XMFLOAT3(2, 1, 2);
    color = DirectX::XMFLOAT4(0, 0, 1, 1);
    //モデルの読み込み
    const char*filename = "Data/FBX/000_cube.fbx";
    std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
    FbxLoader fbx_loader;
    fbx_loader.Load(filename, *model_data);
    std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
    obj = std::make_shared<Model>(model_resource);

    //obj_Render = std::make_shared<ModelRenderer>(device);
    exist = false;
   // speed = DirectX::XMFLOAT2(1.0f, 3.0f);
}
void Shot::init(DirectX::XMFLOAT3 p, DirectX::XMFLOAT3 angle) {
    pos = p;
    speed = DirectX::XMFLOAT2(sinf(angle.y)*1.0, cosf(angle.y)*1.0);
    this->angle = angle;
    exist = true;
}
void Shot::Update() {
    /* pos = p;
    speed = DirectX::XMFLOAT2(sinf(angle.y)*1.0, cosf(angle.y)*1.0);
    this->angle = angle;*/
    switch (Type) {
    case S_TYPE::SType:
        SetColor(DirectX::XMFLOAT4(0, 0, 1, 1));
        break;
    case S_TYPE::NType:
        SetColor(DirectX::XMFLOAT4(1, 0, 0, 1));
        break;
    }
    Move();
    //ワールド変換行列の初期化
    DirectX::XMMATRIX s, r, t;
    //拡大行列作成
    s = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
    //回転行列作成
    r = DirectX::XMMatrixRotationRollPitchYaw(this->angle.x, this->angle.y, this->angle.z);
    //移動行列作成
    t = DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);
    //行列合成と変換
    DirectX::XMStoreFloat4x4(&world, s * r * t);
    DirectX::XMMATRIX WORLD = DirectX::XMLoadFloat4x4(&world);
    obj->CalculateLocalTransform();
    obj->CalculateWorldTransform(WORLD);

    //    Move();
    // exist = true;
}
void Shot::Move() {
    pos.x += speed.x;
    pos.y += 0.0;
    pos.z += speed.y;


}
//void Shot::Render(ID3D11DeviceContext* context,
//    float elapsed_time, const DirectX::XMFLOAT4X4 &view,
//    const DirectX::XMFLOAT4&light_direction, const DirectX::XMFLOAT4& color)
//{
//    //ワールド行列を作成
//    DirectX::XMMATRIX W;
//    {
//
//        DirectX::XMMATRIX s, r, t;
//        s = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);//スケール行列
//        r = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);//回転行列
//        t = DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);//移動行列
//
//        W = s*r*t;
//    }
//
//    
//    obj->CalculateLocalTransform();
//    obj->CalculateWorldTransform(W);
//
//    obj_Render->Begin(context, view, light_direction);
//    obj_Render->Render(context, *obj, color);
//    obj_Render->End(context);
//}

void S_Shot::Init(ID3D11Device* device, std::shared_ptr<ModelResource>& resource) {
    pos = DirectX::XMFLOAT3(0, 0, 0);
    angle = DirectX::XMFLOAT3(0, 0, 0);
    scale = DirectX::XMFLOAT3(1, 1, 2);
    color = DirectX::XMFLOAT4(0, 0, 1, 1);

    obj = std::make_shared<Model>(resource);
    obj_Render = std::make_shared<ModelRenderer>(device);
    exist = false;
}
void S_Shot::Set(DirectX::XMFLOAT3 p, DirectX::XMFLOAT3 angle) {
    pos = p;
    speed = DirectX::XMFLOAT2(sinf(angle.y)*1.0, cosf(angle.y)*1.0);
    this->angle = angle;
    exist = true;
}
void S_Shot::Update() {
    switch (Type) {
    case S_TYPE::SType:
        SetColor(DirectX::XMFLOAT4(0, 0, 1, 1));
        break;
    case S_TYPE::NType:
        SetColor(DirectX::XMFLOAT4(1, 0, 0, 1));
        break;
    }
    pos.x += speed.x;
    pos.y += 0.0;
    pos.z += speed.y;
    //ワールド変換行列の初期化
    DirectX::XMMATRIX s, r, t;
    //拡大行列作成
    s = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
    //回転行列作成
    r = DirectX::XMMatrixRotationRollPitchYaw(this->angle.x, this->angle.y, this->angle.z);
    //移動行列作成
    t = DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);
    //行列合成と変換
    DirectX::XMStoreFloat4x4(&world, s * r * t);
    DirectX::XMMATRIX WORLD = DirectX::XMLoadFloat4x4(&world);
    obj->CalculateLocalTransform();
    obj->CalculateWorldTransform(WORLD);
}

void ShotManager::Init(ID3D11Device* device) {
    //モデルの読み込み
    const char*filename = "Data/FBX/ball.fbx";
    std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
    FbxLoader fbx_loader;
    fbx_loader.Load(filename, *model_data);
    model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
    for (int i = 0; i < ShotMAX; i++) {
        S_Shot *it = &bullet[i];
        it->Init(device, model_resource);
    }
}
void ShotManager::Set(DirectX::XMFLOAT3 p, DirectX::XMFLOAT3 angle,int c) {
    /*for (int i = 0; i < ShotMAX; i++) {
        S_Shot *it = &bullet[i];
        it->Set(p, angle);    
    }*/
    S_Shot *it = &bullet[c];
    it->Set(p, angle);
    /*for (auto& it : bullet) {
        if (!it.isAlive()) {
            it.Set(p, angle);
        }
    }*/
}
void ShotManager::SetType(S_Shot::S_TYPE type, int c) {
    S_Shot *it = &bullet[c];
    it->SetType(type);
}
void ShotManager::Update() {
    for (int i = 0; i < ShotMAX; i++) {
        S_Shot *it = &bullet[i];
        if (it->exist) {
            it->Update();
        }
    }
    /*for (auto& it : bullet) {
        if (it.isAlive()) {
            it.Update();
        }
    }*/
}
void ShotManager::Render(ModelRenderer* renderer, ID3D11DeviceContext* context, float elapsed_time,
    const DirectX::XMFLOAT4X4 &view, const DirectX::XMFLOAT4&light_direction)
{
    for (int i = 0; i < ShotMAX; i++) {
        S_Shot *it = &bullet[i];
        if (it->exist) {
            Model& model = it->GetModel();
            renderer->Render(context, model, it->GetColor());
            //it->Render(context, elapsed_time, view, light_direction);
        }
    }
    /*for (auto&it : bullet) {
        if (it.isAlive()) {
            it.Render(context, elapsed_time, view, light_direction);
        }
    }*/
}
bool ShotCheck::Check() {
    if (OutAmmo)
        return true;
    else
        return false;
}
int ShotCheck::CheckAmmo(int c) {
    if (c == 5) {
        OutAmmo = true;
    }
    if (OutAmmo) {
        Reload();
        return c = 0;
    }
}
void ShotCheck::Reload() {
    timer++;
    if (timer >= 90) {
        timer = 0;
    OutAmmo = false;
    }
}