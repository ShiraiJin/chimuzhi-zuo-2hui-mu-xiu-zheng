#include "Filed.h"
#include "fbx_load.h"

fieldS::fieldS(ID3D11Device*device)
{
    pos = DirectX::XMFLOAT3(1,0,0);
    angle = DirectX::XMFLOAT3(0, 0, 0);
    scale = DirectX::XMFLOAT3(3.0, 3.0, 3.0);
    color = DirectX::XMFLOAT4(1, 0, 0, 0.1);
    
    const char*filename = "Data/FBX/ball.fbx";
    std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
    FbxLoader fbx_loader;
    fbx_loader.Load(filename, *model_data);
    std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
    obj = std::make_shared<Model>(model_resource);

    obj_Render = std::make_shared<ModelRenderer>(device);
}


void  fieldS::Update(float elapsedtime)
{
    //ワールド行列を作成
    DirectX::XMMATRIX W;
    {

        DirectX::XMMATRIX s, r, t;
        s = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);//スケール行列
        r = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);//回転行列
        t = DirectX::XMMatrixTranslation(this->pos.x, this->pos.y, this->pos.z);//移動行列

        W = s*r*t;
    }

    obj->CalculateLocalTransform();
    obj->CalculateWorldTransform(W);
}


void fieldS::Render(ID3D11DeviceContext* context, float elapsed_time,const DirectX::XMFLOAT4X4 &view,
    const DirectX::XMFLOAT4&light_direction)
{

    obj_Render->Begin(context, view, DirectX::XMFLOAT4(0.f, -1, 0.5f, 0));
    obj_Render->Render(context, *obj, color);
    obj_Render->End(context);

}

void fieldS::setpos(DirectX::XMFLOAT3 pos)
{
    this->pos = pos;
}