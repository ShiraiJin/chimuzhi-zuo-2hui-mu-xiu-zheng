#include"skinned_mesh.h"
#include"misc.h"

#include<functional>
#include"ResourceManager.h"
#include"WICTextureLoader.h"
#include<string.h>
//#include<DirectXMath.h>
struct bone_influence
{
	int index = 0; // index of bone 
	float weight = 0; // weight of bone 
};
typedef std::vector<bone_influence> bone_influences_per_control_point;
void fetch_bone_influences(const FbxMesh *fbx_mesh, std::vector<bone_influences_per_control_point> &influences);
void fetch_bone_matrices(FbxMesh *fbx_mesh, std::vector<skinned_mesh::bone> &skeletal, FbxTime time);
void fetch_animations(FbxMesh *fbx_mesh, skinned_mesh::skeletal_animation &skeletal_animation, u_int sampling_rate);

using namespace fbxsdk; 
//コンストラクト
skinned_mesh::skinned_mesh(ID3D11Device * device, const char *fbx_filename, bool boneflag)
{
	HRESULT hr;
	// Create the FBX SDK manager 
	FbxManager* manager = FbxManager::Create();


	// Create an IOSettings object. IOSROOT is defined in Fbxiosettingspath.h. 
	manager->SetIOSettings(FbxIOSettings::Create(manager, IOSROOT));


	// Create an importer. 
	FbxImporter* importer = FbxImporter::Create(manager, "");


	// Initialize the importer. 
	bool import_status = false;
	import_status = importer->Initialize(fbx_filename, -1, manager->GetIOSettings());
	_ASSERT_EXPR_A(import_status, importer->GetStatus().GetErrorString());


	// Create a new scene so it can be populated by the imported file. 
	FbxScene* scene = FbxScene::Create(manager, "");


	// Import the contents of the file into the scene. 
	import_status = importer->Import(scene);
	_ASSERT_EXPR_A(import_status, importer->GetStatus().GetErrorString());

	// Convert mesh, NURBS and patch into triangle mesh 
	fbxsdk::FbxGeometryConverter geometry_converter(manager);
	geometry_converter.Triangulate(scene, /*replace*/true);


	// Fetch node attributes and materials under this node recursively. Currently only mesh.  
	std::vector<FbxNode*> fetched_meshes;
	std::function<void(FbxNode*)> traverse = [&](FbxNode* node) {
		if (node) {
			FbxNodeAttribute *fbx_node_attribute = node->GetNodeAttribute();
			if (fbx_node_attribute) {
				switch (fbx_node_attribute->GetAttributeType()) {
				case FbxNodeAttribute::eMesh:
					fetched_meshes.push_back(node);
					break;
				}
			}
			for (int i = 0; i < node->GetChildCount(); i++)
				traverse(node->GetChild(i));
		}
	};
	traverse(scene->GetRootNode());

	meshes.resize(fetched_meshes.size());
	for (size_t i = 0; i < fetched_meshes.size(); i++)
	{
		FbxMesh *fbx_mesh = fetched_meshes.at(i)->GetMesh();  // Currently only one mesh. 
		mesh&mesh = meshes.at(i);

																	   // Fetch mesh data 
		std::vector<vertex> vertices; // Vertex buffer 
		std::vector<u_int> indices;  // Index buffer 
		u_int vertex_count = 0;
		//UNIT18
		const int number_of_materials = fbx_mesh->GetNode()->GetMaterialCount();
		mesh.subsets.resize(number_of_materials); // UNIT.18 
		if (mesh.subsets.size() == 0)mesh.subsets.resize(1);
		if (number_of_materials > 0)
		{
			const int number_of_polygons = fbx_mesh->GetPolygonCount();
			for (int index_of_polygon = 0; index_of_polygon < number_of_polygons; ++index_of_polygon)
			{
				const u_int material_index = fbx_mesh->GetElementMaterial()->GetIndexArray().GetAt(index_of_polygon);
				mesh.subsets.at(material_index).index_count += 3;
			}
			int offset = 0;
			for (subset &subset : mesh.subsets)
			{
				subset.index_start = offset;
				offset += subset.index_count;
				subset.index_count = 0;
			}
		}
		//UNIT20
		std::vector<bone_influences_per_control_point> bone_influences;
		fetch_bone_influences(fbx_mesh, bone_influences);

		const FbxVector4 *array_of_control_points = fbx_mesh->GetControlPoints();
		const int number_of_polygons = fbx_mesh->GetPolygonCount();
		indices.resize(number_of_polygons * 3); // UNIT.18 
												//UNIT 17
		FbxStringList uv_names;
		fbx_mesh->GetUVSetNames(uv_names);
		wchar_t f[256];
		//int i = fbx_mesh->GetElementNormalCount();
		int texcoord_num = fbx_mesh->GetElementUVCount();
		//UNIT 16
//頂点座標、法線ベクトル、テクスチャ座標の取得
		for (int index_of_polygon = 0; index_of_polygon < number_of_polygons; index_of_polygon++) {
			int index_of_material = 0;
			if (number_of_materials > 0)
			{
				index_of_material = fbx_mesh->GetElementMaterial()->GetIndexArray().GetAt(index_of_polygon);
			}
			subset &subset = mesh.subsets.at(index_of_material); // UNIT.18 
			const int index_offset = subset.index_start + subset.index_count;
			for (int index_of_vertex = 0; index_of_vertex < 3; index_of_vertex++) {
				vertex vertex;
				const int index_of_control_point = fbx_mesh->GetPolygonVertex(index_of_polygon, index_of_vertex);
				vertex.position.x = static_cast<float>(array_of_control_points[index_of_control_point][0]);
				vertex.position.y = static_cast<float>(array_of_control_points[index_of_control_point][1]);
				vertex.position.z = static_cast<float>(array_of_control_points[index_of_control_point][2]);


				FbxVector4 normal;
				fbx_mesh->GetPolygonVertexNormal(index_of_polygon, index_of_vertex, normal);
				vertex.normal.x = static_cast<float>(normal[0]);
				vertex.normal.y = static_cast<float>(normal[1]);
				vertex.normal.z = static_cast<float>(normal[2]);

				//UNIT 17
				if (texcoord_num > 0)
				{
					FbxVector2 uv;
					bool unmapped_uv;
					fbx_mesh->GetPolygonVertexUV(index_of_polygon, index_of_vertex, uv_names[0], uv, unmapped_uv);
					vertex.texcoord.x = static_cast<float>(uv[0]);
					vertex.texcoord.y = 1.0f - static_cast<float>(uv[1]);

				}
				else
				{
					vertex.texcoord.x = 0.f;
					vertex.texcoord.y = 0.f;
				}
				int a = 0;
				//for (auto&bone : bone_influences.at(index_of_control_point))
				//{
				//	vertex.bone_indices[a] = bone.index;
				//	vertex.bone_weights[a] = bone.weight;
				//	if(a<3)a++;
				//}
				float  bone_weight = 0.f;

				for (auto&bone : bone_influences.at(index_of_control_point))
				{
					if (a < 3)
					{
						vertex.bone_indices[a] = bone.index;
						vertex.bone_weights[a] = bone.weight;
						a++;
						bone_weight += bone.weight;
					}
					else
					{
						vertex.bone_indices[a] = bone.index;
						vertex.bone_weights[a] = 1 - bone_weight;

					}
				}

				vertices.push_back(vertex);
				//indices.push_back(vertex_count);
				indices.at(index_offset + index_of_vertex) = static_cast<u_int>(vertex_count);
				vertex_count += 1;

			}
			subset.index_count += 3;
		}


		//UNIT17
//カラーやテクスチャ情報の取得
		for (int index_of_material = 0; index_of_material < number_of_materials; ++index_of_material)
		{
			subset &subset = mesh.subsets.at(index_of_material); // UNIT.18 
			const FbxSurfaceMaterial *surface_material = fbx_mesh->GetNode()->GetMaterial(index_of_material);
			const FbxProperty property = surface_material->FindProperty(FbxSurfaceMaterial::sDiffuse);
			const FbxProperty factor = surface_material->FindProperty(FbxSurfaceMaterial::sDiffuseFactor);
			if (property.IsValid() && factor.IsValid())
			{
				FbxDouble3 color = property.Get<FbxDouble3>();
				double f = factor.Get<FbxDouble>();
				subset.diffuse.color.x = static_cast<float>(color[0] * f);
				subset.diffuse.color.y = static_cast<float>(color[1] * f);
				subset.diffuse.color.z = static_cast<float>(color[2] * f);
				subset.diffuse.color.w = 1.0f;
			}
			if (property.IsValid())
			{
				const int number_of_textures = property.GetSrcObjectCount<FbxFileTexture>();
				if (number_of_textures)
				{
					const FbxFileTexture* file_texture = property.GetSrcObject<FbxFileTexture>();
					if (file_texture)
					{
						errno_t err;
						const char *filename = file_texture->GetRelativeFileName();
						char t_file[256];
						char texture_file[256] = { 0 };
						char t[256];
						err = _splitpath_s(filename, nullptr, 0, nullptr, 0, t_file, sizeof(t_file) / sizeof(t_file[0]), t, sizeof(t) / sizeof(t[0]));
						if (err != 0)
						{
							assert(0);
						}
						strcpy_s(texture_file,256, t_file);
						strcat_s(texture_file,256, t);
						
						char dir[256];
						err = _splitpath_s(fbx_filename, nullptr, 0, dir, sizeof(dir) / sizeof(dir[0]), nullptr, 0, nullptr, 0);
						if (err != 0)
						{
							assert(0);
						}
						char path_buffer[256];

						err = _makepath_s(path_buffer, sizeof(path_buffer) / sizeof(path_buffer[0]), nullptr, dir, texture_file, nullptr);
						if (err != 0)
						{
							assert(0);
						}

						size_t s = 0;
						mbstowcs_s(&s, f, 256, path_buffer, _TRUNCATE);
						const wchar_t*f_name = f;

						hr = pResourceManager.LoadTexture(device, f_name, &subset.diffuse.shader_resource_view, &subset.diffuse.texture2d_desc);
						_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

					}

				}
			}

		}

		for (auto&sub : mesh.subsets)
		{
			if (sub.diffuse.shader_resource_view == nullptr)
			{
				//ダミーテクスチャ作成
				static unsigned char image[]{ 255,255,255 };
				D3D11_TEXTURE2D_DESC desc;
				D3D11_SUBRESOURCE_DATA initialData;
				ID3D11Texture2D* texture = NULL;

				//ZeroMemory(&desc, sizeof(D3D11_TEXTURE2D_DESC));
				desc.Width = 1;
				desc.Height = 1;
				desc.MipLevels = 1;
				desc.ArraySize = 1;
				desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
				desc.SampleDesc.Count = 1;
				desc.SampleDesc.Quality = 0;
				desc.Usage = D3D11_USAGE_DEFAULT;
				desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
				desc.CPUAccessFlags = 0;
				desc.MiscFlags = 0;

				initialData.pSysMem = image;
				initialData.SysMemPitch = 1;
				initialData.SysMemSlicePitch = 0;

				hr = device->CreateTexture2D(&desc, &initialData, &texture);
				_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
				ID3D11ShaderResourceView* textureRview = NULL;
				//ID3DX11EffectShaderResourceVariable* textureRVar = NULL;

				D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
				ZeroMemory(&SRVDesc, sizeof(SRVDesc));
				SRVDesc.Format = desc.Format;
				SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				SRVDesc.Texture2D.MostDetailedMip = 0;
				SRVDesc.Texture2D.MipLevels = 1;

				ID3D11Resource*resource = nullptr;
				const D3D11_SHADER_RESOURCE_VIEW_DESC shader_resoirce_view_desc;
				hr = device->CreateShaderResourceView(texture, &SRVDesc, &sub.diffuse.shader_resource_view);
				_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

			}

		}
		FbxAMatrix global_transform = fbx_mesh->GetNode()->EvaluateGlobalTransform(0);
		for (int row = 0; row < 4; row++)
		{
			for (int column = 0; column < 4; column++)
			{
				mesh.global_transform(row,column) = static_cast<float>(global_transform[row][column]);
			}
		}
		// 頂点バッファの生成
		{
			// 頂点バッファを作成するための設定オプション
			D3D11_BUFFER_DESC buffer_desc = {};
			buffer_desc.ByteWidth = sizeof(vertex)*vertices.size();	// バッファ（データを格納する入れ物）のサイズ
			buffer_desc.Usage = D3D11_USAGE_DEFAULT;
			buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;	// 頂点バッファとしてバッファを作成する。
			buffer_desc.CPUAccessFlags = 0;
			buffer_desc.MiscFlags = 0;
			buffer_desc.StructureByteStride = 0;
			// 頂点バッファに頂点データを入れるための設定
			D3D11_SUBRESOURCE_DATA subresource_data = {};
			subresource_data.pSysMem = vertices.data();	// ここに格納したい頂点データのアドレスを渡すことでCreateBuffer()時にデータを入れることができる。
			subresource_data.SysMemPitch = 0; //Not use for vertex buffers.
			subresource_data.SysMemSlicePitch = 0; //Not use for vertex buffers.
												   // 頂点バッファオブジェクトの生成
			hr = device->CreateBuffer(&buffer_desc, &subresource_data, &mesh.vertex_buffer);
			_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		}
		// インテックスバッファの生成
		{
			// インテックスバッファを作成するための設定オプション
			D3D11_BUFFER_DESC buffer_desc = {};
			buffer_desc.ByteWidth = sizeof(u_int)*indices.size();	// バッファ（データを格納する入れ物）のサイズ
			buffer_desc.Usage = D3D11_USAGE_DEFAULT;//またはD3D11_USAGE_IMMUTABLE
			buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;	// インテックスバッファとしてバッファを作成する。
			buffer_desc.CPUAccessFlags = 0;
			buffer_desc.MiscFlags = 0;
			buffer_desc.StructureByteStride = 0;
			// インテックスバッファにインテックスデータを入れるための設定
			D3D11_SUBRESOURCE_DATA subresource_data = {};
			subresource_data.pSysMem = indices.data();	// ここに格納したいインテックスデータのアドレスを渡すことでCreateBuffer()時にデータを入れることができる。
			subresource_data.SysMemPitch = 0; //Not use for vertex buffers.
			subresource_data.SysMemSlicePitch = 0; //Not use for vertex buffers.
												   // インテックスバッファオブジェクトの生成
			hr = device->CreateBuffer(&buffer_desc, &subresource_data, &mesh.index_buffer);
			_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		}
		//UNIT22
		//FbxTime::EMode time_mode = fbx_mesh->GetScene()->GetGlobalSettings().GetTimeMode();
		//FbxTime frame_time;
		//frame_time.SetTime(0, 0, 0, 1, 0, time_mode);
		//fetch_bone_matrices(fbx_mesh, mesh.skeletal, frame_time * 20); // pose at frame 20 
		//UNIT23
		fetch_animations(fbx_mesh, mesh.skeletal_animation, 0);

	}
	// Initialize Direct3D COM objects 

	manager->Destroy();

	createbuffers(device,boneflag);
}
//ボーン影響度の取得
void fetch_bone_influences(const FbxMesh * fbx_mesh, std::vector<bone_influences_per_control_point>& influences)
{
	const int number_of_control_points = fbx_mesh->GetControlPointsCount();
	influences.resize(number_of_control_points);

	const int number_of_deformers = fbx_mesh->GetDeformerCount(FbxDeformer::eSkin);
	for (int index_of_deformer = 0; index_of_deformer < number_of_deformers; ++index_of_deformer)
	{
		FbxSkin *skin = static_cast<FbxSkin *>(fbx_mesh->GetDeformer(index_of_deformer, FbxDeformer::eSkin));

		const int number_of_clusters = skin->GetClusterCount();
		for (int index_of_cluster = 0; index_of_cluster < number_of_clusters; ++index_of_cluster)
		{
			FbxCluster* cluster = skin->GetCluster(index_of_cluster);

			const int number_of_control_point_indices = cluster->GetControlPointIndicesCount();
			const int *array_of_control_point_indices = cluster->GetControlPointIndices();
			const double *array_of_control_point_weights = cluster->GetControlPointWeights();

			for (int i = 0; i < number_of_control_point_indices; ++i)
			{
				bone_influences_per_control_point &influences_per_control_point
					= influences.at(array_of_control_point_indices[i]);
				bone_influence influence;
				influence.index = index_of_cluster;
				influence.weight = static_cast<float>(array_of_control_point_weights[i]);
				influences_per_control_point.push_back(influence);
			}
		}
	}
}
//ボーン行列を取得
void fetch_bone_matrices(FbxMesh *fbx_mesh, std::vector<skinned_mesh::bone> &skeletal, FbxTime time)
{
	const int number_of_deformers = fbx_mesh->GetDeformerCount(FbxDeformer::eSkin);
	for (int index_of_deformer = 0; index_of_deformer < number_of_deformers; ++index_of_deformer)
	{
		FbxSkin *skin = static_cast<FbxSkin *>(fbx_mesh->GetDeformer(index_of_deformer, FbxDeformer::eSkin));

		const int number_of_clusters = skin->GetClusterCount();
		skeletal.resize(number_of_clusters);
		for (int index_of_cluster = 0; index_of_cluster < number_of_clusters; ++index_of_cluster)
		{
			skinned_mesh::bone &bone = skeletal.at(index_of_cluster);

			FbxCluster *cluster = skin->GetCluster(index_of_cluster);

			// this matrix trnasforms coordinates of the initial pose from mesh space to global space 

			FbxAMatrix reference_global_init_position;
			cluster->GetTransformMatrix(reference_global_init_position);

			// this matrix trnasforms coordinates of the initial pose from bone space to global space 
			FbxAMatrix cluster_global_init_position;
			cluster->GetTransformLinkMatrix(cluster_global_init_position);

			// this matrix trnasforms coordinates of the current pose from bone space to global space 
			FbxAMatrix cluster_global_current_position;
			cluster_global_current_position = cluster->GetLink()->EvaluateGlobalTransform(time);

			// this matrix trnasforms coordinates of the current pose from mesh space to global space 
			FbxAMatrix reference_global_current_position;
			reference_global_current_position = fbx_mesh->GetNode()->EvaluateGlobalTransform(time);

			// Matrices are defined using the Column Major scheme. When a FbxAMatrix represents a transformation 
			// (translation, rotation and scale), the last row of the matrix represents the translation part of the 
			// transformation. 
			FbxAMatrix transform = reference_global_current_position.Inverse() * cluster_global_current_position
				* cluster_global_init_position.Inverse() * reference_global_init_position;
			// convert FbxAMatrix(transform) to XMDLOAT4X4(bone.transform) 
			float *transform_pos = &bone.transform._11;
			for (int x = 0; x < 4; x++)
			{
				for (int y = 0; y < 4; y++)
				{
					*transform_pos = transform.Get(x, y);
					transform_pos++;
				}
			}

		}
	}
}
//アニメーションを取得
void fetch_animations(FbxMesh *fbx_mesh, skinned_mesh::skeletal_animation &skeletal_animation, u_int sampling_rate = 0)
{
	// Get the list of all the animation stack. 
	FbxArray<FbxString *> array_of_animation_stack_names;
	fbx_mesh->GetScene()->FillAnimStackNameArray(array_of_animation_stack_names);
	// Get the number of animations. 
	int number_of_animations = array_of_animation_stack_names.Size();
	if (number_of_animations > 0)
	{
		// Get the FbxTime per animation's frame. 
		FbxTime::EMode time_mode = fbx_mesh->GetScene()->GetGlobalSettings().GetTimeMode();
		FbxTime frame_time;
		frame_time.SetTime(0, 0, 0, 1, 0, time_mode);

		sampling_rate = sampling_rate > 0 ? sampling_rate : frame_time.GetFrameRate(time_mode);
		float sampling_time = 1.0f / sampling_rate;
		skeletal_animation.sampling_time = sampling_time;
		skeletal_animation.animation_tick = 0.0f;

		FbxString *animation_stack_name = array_of_animation_stack_names.GetAt(0);
		FbxAnimStack * current_animation_stack = fbx_mesh->GetScene()->FindMember<FbxAnimStack>(animation_stack_name->Buffer());
		fbx_mesh->GetScene()->SetCurrentAnimationStack(current_animation_stack);

		FbxTakeInfo *take_info = fbx_mesh->GetScene()->GetTakeInfo(animation_stack_name->Buffer());
		FbxTime start_time = take_info->mLocalTimeSpan.GetStart();
		FbxTime end_time = take_info->mLocalTimeSpan.GetStop();

		FbxTime sampling_step;
		sampling_step.SetTime(0, 0, 1, 0, 0, time_mode);
		sampling_step = static_cast<FbxLongLong>(sampling_step.Get() * sampling_time);
		for (FbxTime current_time = start_time; current_time < end_time; current_time += sampling_step)
		{
			skinned_mesh::skeletal skeletal;
			fetch_bone_matrices(fbx_mesh, skeletal, current_time);
			skeletal_animation.push_back(skeletal);
		}
		for (FbxTime current_time = start_time; current_time < end_time; current_time += sampling_step)
		{
			skinned_mesh::skeletal skeletal;
			fetch_bone_matrices(fbx_mesh, skeletal, current_time);
			skeletal_animation.push_back(skeletal);
		}
	}
	for (int i = 0; i < number_of_animations; i++)
	{
		delete array_of_animation_stack_names[i];
	}
}
//シェーダーなどの初期設定
void skinned_mesh::createbuffers(ID3D11Device * device, bool boneflag)
{
	HRESULT hr = S_OK;

	// 定数バッファの生成
	{
		// 定数バッファを作成するための設定オプション
		D3D11_BUFFER_DESC buffer_desc = {};
		buffer_desc.ByteWidth = sizeof(cbuffer);	// バッファ（データを格納する入れ物）のサイズ
		buffer_desc.Usage = D3D11_USAGE_DEFAULT;
		buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;	// 定数バッファとしてバッファを作成する。
		buffer_desc.CPUAccessFlags = 0;
		buffer_desc.MiscFlags = 0;
		buffer_desc.StructureByteStride = 0;
		hr = device->CreateBuffer(&buffer_desc, nullptr, &constant_buffer);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	}

	// 頂点シェーダー＆頂点入力レイアウトの生成
	{
		// 頂点シェーダーファイルを開く(sprite_vs.hlsl をコンパイルしてできたファイル)
		FILE* fp = nullptr;
		fopen_s(&fp, "Data/shader/skinned_mesh_vs.cso", "rb");
		_ASSERT_EXPR_A(fp, "CSO File not found");

		// 頂点シェーダーファイルのサイズを求める
		fseek(fp, 0, SEEK_END);
		long cso_sz = ftell(fp);
		fseek(fp, 0, SEEK_SET);

		// メモリ上に頂点シェーダーデータを格納する領域を用意する
		unsigned char* cso_data = new unsigned char[cso_sz];
		fread(cso_data, cso_sz, 1, fp);	// 用意した領域にデータを読み込む
		fclose(fp);	// ファイルを閉じる

					// 頂点シェーダーデータを基に頂点シェーダーオブジェクトを生成する
		HRESULT hr = device->CreateVertexShader(
			cso_data,		// 頂点シェーダーデータのポインタ
			cso_sz,			// 頂点シェーダーデータサイズ
			nullptr,
			&vertex_shader	// 頂点シェーダーオブジェクトのポインタの格納先。
		);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

		// GPUに頂点データの内容を教えてあげるための設定
		D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BONES", 0, DXGI_FORMAT_R32G32B32A32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};

		// 入力レイアウトの生成
		hr = device->CreateInputLayout(
			input_element_desc,				// 頂点データの内容
			ARRAYSIZE(input_element_desc),	// 頂点データの要素数
			cso_data,						// 頂点シェーダーデータ（input_element_descの内容と sprite_vs.hlslの内容に不一致がないかチェックするため）
			cso_sz,							// 頂点シェーダーデータサイズ
			&input_layout					// 入力レイアウトオブジェクトのポインタの格納先。
		);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

		// 頂点シェーダーデータの後始末
		delete[] cso_data;
	}

	// ピクセルシェーダーの生成
	{
		// ピクセルシェーダーファイルを開く(sprite_ps.hlsl をコンパイルしてできたファイル)
		FILE* fp = nullptr;
		fopen_s(&fp, "Data/shader/skinned_mesh_ps.cso", "rb");
		_ASSERT_EXPR_A(fp, "CSO File not found");

		// ピクセルシェーダーファイルのサイズを求める
		fseek(fp, 0, SEEK_END);
		long cso_sz = ftell(fp);
		fseek(fp, 0, SEEK_SET);

		// メモリ上にピクセルシェーダーデータを格納する領域を用意する
		unsigned char* cso_data = new unsigned char[cso_sz];
		fread(cso_data, cso_sz, 1, fp);	// 用意した領域にデータを読み込む
		fclose(fp);	// ファイルを閉じる

					// ピクセルシェーダーの生成
		HRESULT hr = device->CreatePixelShader(
			cso_data,		// ピクセルシェーダーデータのポインタ
			cso_sz,			// ピクセルシェーダーデータサイズ
			nullptr,
			&pixel_shader	// ピクセルシェーダーオブジェクトのポインタの格納先
		);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

		// ピクセルシェーダーデータの後始末
		delete[] cso_data;
	}

	// ラスタライザステートの生成
	{
		// ラスタライザステートを作成するための設定オプション
		D3D11_RASTERIZER_DESC rasterizer_desc = {}; //https://msdn.microsoft.com/en-us/library/windows/desktop/ff476198(v=vs.85).aspx
		rasterizer_desc.FillMode = D3D11_FILL_SOLID; //D3D11_FILL_WIREFRAME, D3D11_FILL_SOLID
		rasterizer_desc.CullMode = D3D11_CULL_BACK; //D3D11_CULL_NONE：両面描画, D3D11_CULL_FRONT, D3D11_CULL_BACK   
		rasterizer_desc.FrontCounterClockwise = boneflag;//これがFALSEなら時計回り、TRUEなら反時計回り
		rasterizer_desc.DepthBias = 0; //https://msdn.microsoft.com/en-us/library/windows/desktop/cc308048(v=vs.85).aspx
		rasterizer_desc.DepthBiasClamp = 0;
		rasterizer_desc.SlopeScaledDepthBias = 0;
		rasterizer_desc.DepthClipEnable = FALSE;
		rasterizer_desc.ScissorEnable = FALSE;
		rasterizer_desc.MultisampleEnable = FALSE;
		rasterizer_desc.AntialiasedLineEnable = FALSE;
		//if (boneflag)
		//{
		//	coordinate_conversion=DirectX::XMFLOAT4X4
		//	{
		//		1, 0, 0, 0,
		//		0, 1, 0, 0,
		//		0, 0, 1, 0,
		//		0, 0, 0, 1
		//	};
		//}
		// ラスタライザステートの生成
		hr = device->CreateRasterizerState(
			&rasterizer_desc,
			&rasterizer_state
		);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	}
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = -FLT_MAX;
	sampDesc.MaxLOD = FLT_MAX;
	sampDesc.MaxAnisotropy = 16;
	memcpy(sampDesc.BorderColor, &DirectX::XMFLOAT4(1.0f, 1.f, 1.f, 1.f), sizeof(DirectX::XMFLOAT4));
	hr = device->CreateSamplerState(&sampDesc, &samplerstate);
	if (FAILED(hr)) {
		assert(0);
	}

}

//描画
void skinned_mesh::render(ID3D11DeviceContext *immediate_context,
	const DirectX::XMFLOAT4&color,
	const DirectX::XMFLOAT4& light_direction,
	const DirectX::XMFLOAT4X4&world_view_projection,
	const DirectX::XMFLOAT4X4&world,
	float elapsed_time)
{
	// 使用する頂点バッファやシェーダーなどをGPUに教えてやる。
	UINT stride = sizeof(vertex);
	UINT offset = 0;
	immediate_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);	// ポリゴンの描き方の指定
	immediate_context->IASetInputLayout(input_layout);
	immediate_context->VSSetShader(vertex_shader, nullptr, 0);
	immediate_context->PSSetShader(pixel_shader, nullptr, 0);
	immediate_context->RSSetState(rasterizer_state);
	immediate_context->PSSetSamplers(0, 1, &samplerstate);

	for(auto &mesh : meshes)
	{
		
		immediate_context->IASetVertexBuffers(0, 1, mesh.vertex_buffer.GetAddressOf(), &stride, &offset);
		immediate_context->IASetIndexBuffer(*mesh.index_buffer.GetAddressOf(), DXGI_FORMAT_R32_UINT, 0);//DXGI_FORMAT_R32_UINT=unsigned intの32ビット
		cbuffer data;
		if (mesh.skeletal_animation.size() > 0)
		{
			
			int frame = mesh.skeletal_animation.animation_tick / mesh.skeletal_animation.sampling_time;
			if (frame > mesh.skeletal_animation.size() - 1)
			{
				frame = 0;
				mesh.skeletal_animation.animation_tick = 0;
			}
			std::vector<bone> &skeletal = mesh.skeletal_animation.at(frame);
			size_t number_of_bones = skeletal.size();
			_ASSERT_EXPR(number_of_bones < MAX_BONES, L"'the number_of_bones' exceeds MAX_BONES.");
			for (size_t i = 0; i < number_of_bones; i++)
			{
				XMStoreFloat4x4(&data.bone_transforms[i], XMLoadFloat4x4(&skeletal.at(i).transform));
			}
			mesh.skeletal_animation.animation_tick += elapsed_time;
			data.world_view_projection = world_view_projection;
			data.world = world;
		}
		else
		{
			DirectX::XMStoreFloat4x4(&data.bone_transforms[0], DirectX::XMMatrixIdentity());
			DirectX::XMStoreFloat4x4(&data.bone_transforms[1], DirectX::XMMatrixIdentity());
			DirectX::XMStoreFloat4x4(&data.bone_transforms[2], DirectX::XMMatrixIdentity());
			DirectX::XMStoreFloat4x4(&data.world_view_projection,
				DirectX::XMLoadFloat4x4(&mesh.global_transform) *
				DirectX::XMLoadFloat4x4(&coordinate_conversion) *
				DirectX::XMLoadFloat4x4(&world_view_projection));
			DirectX::XMStoreFloat4x4(&data.world,
				DirectX::XMLoadFloat4x4(&mesh.global_transform) *
				DirectX::XMLoadFloat4x4(&coordinate_conversion) *
				DirectX::XMLoadFloat4x4(&world));
		}
		data.light_direction = light_direction;
		for (auto&sub : mesh.subsets)
		{
			//定数バッファの内容を更新
			{
			
				data.material_color.x = sub.diffuse.color.x/**color.x*/;
				data.material_color.y = sub.diffuse.color.y/**color.y*/;
				data.material_color.z = sub.diffuse.color.z/**color.z*/;
				data.material_color.w = sub.diffuse.color.w;
				immediate_context->UpdateSubresource(constant_buffer, 0, 0, &data, 0, 0);//定数バッファの内容を変えるときこれを使う
				immediate_context->VSSetConstantBuffers(0, 1, &constant_buffer);//頂点シェーダに定数バッファの情報を送る
			}
			immediate_context->PSSetShaderResources(0, 1, &sub.diffuse.shader_resource_view);
			immediate_context->DrawIndexed(sub.index_count, sub.index_start, 0);
		}
		
	}



	// ↑で設定したリソースを利用してポリゴンを描画する。
	//immediate_context->Draw(4, 0);
}
