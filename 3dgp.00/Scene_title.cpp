#include "Scene_title.h"
#include "camera.h"
#include "fbx_load.h"
#include "keyboad.h"


Scene_Title::Scene_Title(ID3D11Device * device)
{



    T_sprite = std::make_unique<Sprite>(device, L"Data/image/Title.png");
    Push_Enter = std::make_unique<Sprite>(device, L"Data/image/PLEASE_ENTER.png");
    Title_s = std::make_unique<Sound>("Data/BGM/Titile.wav");
    Counter = 0;
}

int Scene_Title::Update(float elapsed_time)
{
    if (!Title_s->Playing())
    {
        Title_s->Play(true);
        Title_s->SetVolume(1.0f);
    }
    Counter++;

    if (GetAsyncKeyState(VK_RETURN) & 1)
    {
        return 1;
    }
    return 0;
}
void Scene_Title::Render(float elapsed_time, ID3D11DeviceContext* devicecontext)
{
    T_sprite->render(devicecontext, 0, 0, 1920, 1080, 0, 0, 1920, 1080, 0, 1, 1, 1, 1);
    if ((Counter / 40) % 2)
    {
        Push_Enter->render(devicecontext, 50, 800, 683, 58, 0, 0, 683, 58, 0, 1, 1, 1, 1);
    }
}

Scene_Title::~Scene_Title()
{
    //  pCamera.Destroy();
}