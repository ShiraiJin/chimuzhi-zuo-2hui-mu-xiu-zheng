#include <memory.h>
#include <vector>
#include "model.h"
#include "model_renderer.h"


class Shot {
public:
    enum S_TYPE {
        SType,
        NType,
        NUM,
    };
private:
    std::shared_ptr<Model> obj;
    //std::shared_ptr<ModelRenderer> obj_Render;

    DirectX::XMFLOAT4 color;
    DirectX::XMFLOAT3 pos;
    DirectX::XMFLOAT3 angle;
    DirectX::XMFLOAT3 scale;
    DirectX::XMFLOAT2 speed;
    DirectX::XMFLOAT4X4 world;
    
    S_TYPE Type = SType;

public:
    bool exist;
    Shot(ID3D11Device* device);
    void init(DirectX::XMFLOAT3 p, DirectX::XMFLOAT3 angle);
    void Update();
    void Move();
    /*void Render(ID3D11DeviceContext* context, float elapsed_time, const DirectX::XMFLOAT4X4 &view, const DirectX::XMFLOAT4X4 &prolection, const DirectX::XMFLOAT4&light_direction);
    }*/
    void SetType(S_TYPE type) { this->Type = type; };
    void SetPosition(const DirectX::XMFLOAT3& p) { pos = p; };
    void SetScale(const DirectX::XMFLOAT3& s) { scale = s; };
    void SetAngle(const DirectX::XMFLOAT3& a) { angle = a; };
    void SetColor(const DirectX::XMFLOAT4& c) { color = c; };
    const DirectX::XMFLOAT3& GetPosition() { return pos; };
    const DirectX::XMFLOAT3& GetScale() { return scale; };
    const DirectX::XMFLOAT3& GetAngle() { return angle; };
    const DirectX::XMFLOAT4& GetColor() { return color; };
    Model& GetModel() {
        return *obj.get();
    }
};
class S_Shot {
public:
    enum S_TYPE {
        SType,
        NType,
        NUM,
    };
private:
    std::shared_ptr<Model> obj;
    std::shared_ptr<ModelRenderer> obj_Render;
    DirectX::XMFLOAT4 color;
    DirectX::XMFLOAT3 pos;
    DirectX::XMFLOAT3 angle;
    DirectX::XMFLOAT3 scale;
    DirectX::XMFLOAT2 speed;
    DirectX::XMFLOAT4X4 world;



    S_TYPE Type = SType;
public:
    bool exist;
    S_Shot() {};
    void Init(ID3D11Device*device, std::shared_ptr<ModelResource>& resource);
    void Set(DirectX::XMFLOAT3 p, DirectX::XMFLOAT3 angle);
    void Update();
    

    void SetType(S_TYPE type) { this->Type = type; };
    void SetPosition(const DirectX::XMFLOAT3& p) { pos = p; };
    void SetScale(const DirectX::XMFLOAT3& s) { scale = s; };
    void SetAngle(const DirectX::XMFLOAT3& a) { angle = a; };
    void SetColor(const DirectX::XMFLOAT4& c) { color = c; };
    const DirectX::XMFLOAT3& GetPosition() { return pos; };
    const DirectX::XMFLOAT3& GetScale() { return scale; };
    const DirectX::XMFLOAT3& GetAngle() { return angle; };
    const DirectX::XMFLOAT4& GetColor() { return color; };
    Model& GetModel() {
        return *obj.get();
    }
};
class ShotManager {
private:
    const static int ShotMAX = 6;

    S_Shot bullet[ShotMAX];
    std::shared_ptr<ModelResource> model_resource;
public:
    void Init(ID3D11Device*device);
    void Update();
    void Set(DirectX::XMFLOAT3 p, DirectX::XMFLOAT3 angle,int c);
    void Render(ModelRenderer* renderer, ID3D11DeviceContext* context, float elapsed_time,
        const DirectX::XMFLOAT4X4 &view, const DirectX::XMFLOAT4&light_direction);
    void SetType(S_Shot::S_TYPE type, int c);
    const DirectX::XMFLOAT3& GetPosition() {
        for (int i = 0; i < 6; i++) {
            S_Shot *it = &bullet[i];
            if(it->exist)
            return it->GetPosition();
        }
    };
    const DirectX::XMFLOAT3& GetScale() {
        for (int i = 0; i < 6; i++) {
            S_Shot *it = &bullet[i];
            if (it->exist)
                return it->GetScale();
        }
    };
    bool isAlive() {
        for (int i = 0; i < 6; i++) {
            S_Shot *it = &bullet[i];
            return it->exist;
        }
        };

    static ShotManager *getInstance() {
        static ShotManager instance;
        return &instance;
    }
};

#define pShot ShotManager::getInstance()

class ShotCheck {
private:
    bool OutAmmo = false;
    int timer = 0;
public:
    int CheckAmmo(int c);
    void Reload();
    bool Check();
};