#pragma once
#include"scene.h"
#include<memory>
#include <thread>

class Scene_Result:public Scene
{
private:
   


public:

    Scene_Result(ID3D11Device*device);
    int Update(float elapsed_time);
    void Render(float elapsed_time, ID3D11DeviceContext* devicecontext);
    ~Scene_Result();
};