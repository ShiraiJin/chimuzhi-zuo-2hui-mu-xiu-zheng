//float4 main( float4 pos : POSITION ) : SV_POSITION
//{
//	return pos;
//}
#include"static_mesh.hlsli"
VS_OUT main(float4 pos : POSITION, float3 normal : NORMAL, float2 texcoord : TEXCOORD)
{
	VS_OUT vout;
	vout.pos = mul(pos, world_view_projection);
	float3 N = normalize(mul(normal, (float3x3)world));
	float3 L = normalize(-light_direction.xyz);
	float d = dot(L, N);
	vout.color.xyz = material_color.xyz*max(0, d);
	vout.color.w = material_color.w;
	vout.texcoord = texcoord;

	return vout;
}
