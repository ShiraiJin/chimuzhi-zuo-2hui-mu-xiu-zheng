#pragma once
#include<d3d11.h>
#include <DirectXMath.h>
#include "model.h"
#include "model_renderer.h"
#include "Geometric_Primitive.h"
#include "camera.h"
using namespace DirectX;
class fieldS
{
private:
    std::shared_ptr<Model> obj;
    std::shared_ptr<ModelRenderer> obj_Render;
    std::shared_ptr<geometricSphere>Sphere;
    DirectX::XMFLOAT3 angle;
    DirectX::XMFLOAT3 scale;
    DirectX::XMFLOAT4 color;
    DirectX::XMFLOAT4X4 world;
   float speed;
public:
    DirectX::XMFLOAT3 pos;
    bool hit = true;
    fieldS(ID3D11Device*device);
    ~fieldS() {};
    void Update(float elapsed_time);
    void Render(ID3D11DeviceContext* context,float elapsed_time, const DirectX::XMFLOAT4X4 &view,
        const DirectX::XMFLOAT4&light_direction);
    float GetSpeed() const {
        return speed;
    }

    DirectX::XMFLOAT3 GetPos() { return pos; }
    DirectX::XMFLOAT3 Getscale() { return scale; }

    void setpos(DirectX::XMFLOAT3);
};

