#include "scene_manager.h"
#include"Scene_title.h"
#include"Scene_game.h"
#include"Scene_Result.h"
#include"Scene_over.h"
//#include"keyboad.h"

SceneManager*SceneManager::m_scenemanager = nullptr;
SceneManager::SceneManager(ID3D11Device * device)
{

	m_scene = new Scene_Title(device);

}
void SceneManager::Updata(float elapsed_time, ID3D11Device * device)
{

	int scene_chang = m_scene->Update(elapsed_time);
	if (scene_chang > 0)
	{
		ChangScene(scene_chang, device);
	}
}

void SceneManager::ChangScene(int next_scene, ID3D11Device * device)
{
	delete m_scene;
	switch (next_scene)
	{
	case SceneName::Tile:
		m_scene = new Scene_Title(device);

		break;
	case SceneName::Game:
		m_scene = new Scene_Game(device);
		break;
	case SceneName::Result:
		m_scene = new Scene_Result(device);
		break;
	case SceneName::Over:
		m_scene = new Scene_over(device);
		break;
	}

}

void SceneManager::Render(float elapsed_time, ID3D11DeviceContext* devicecontext)
{

	m_scene->Render(elapsed_time, devicecontext);
}
