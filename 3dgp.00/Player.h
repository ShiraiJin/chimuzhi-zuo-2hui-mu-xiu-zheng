#pragma once
#include <memory>
#include "model.h"
#include "model_renderer.h"
#include "Shot.h"
class Player {
public:
    enum TYPE {
        SType,
        NType,
        NUM,
    };
private:
    std::shared_ptr<Model> obj;
    // std::shared_ptr<ModelRenderer> obj_Render;
    std::unique_ptr<Shot> shot;
    DirectX::XMFLOAT4 color;
    DirectX::XMFLOAT3 pos;
    DirectX::XMFLOAT3 angle;
    DirectX::XMFLOAT3 scale;
    //DirectX::XMFLOAT2 speed;
    DirectX::XMFLOAT4X4 world;
    bool exist;

    TYPE Type = TYPE::SType;
    POINT pt;

public:


    Player(ID3D11Device* device);

    void Init();
    void Update();
    void Move();
    void AngleChange();
    /*void Render(ID3D11DeviceContext* context,
    float elapsed_time, const DirectX::XMFLOAT4X4 &view,
    const DirectX::XMFLOAT4&light_direction);*/
    Model& GetModel() {
        return *obj.get();
    }
    void SetType(TYPE type) { this->Type = type; };
    void SetPosition(const DirectX::XMFLOAT3& p) { pos = p; };
    void SetScale(const DirectX::XMFLOAT3& s) { scale = s; };
    void SetAngle(const DirectX::XMFLOAT3& a) { angle = a; };
    void SetColor(const DirectX::XMFLOAT4& c) { color = c; };
    const DirectX::XMFLOAT3& GetPosition() { return pos; };
    const DirectX::XMFLOAT3& GetScale() { return scale; };
    const DirectX::XMFLOAT3& GetAngle() { return angle; };
    const DirectX::XMFLOAT4& GetColor() { return color; };


};

