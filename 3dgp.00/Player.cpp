#include "Player.h"
#include "fbx_load.h"
#include <Windows.h>

Player::Player(ID3D11Device* device) {
    //パラメータ（仮）※後で修正する
    pos = DirectX::XMFLOAT3(0, 0, 0);
    angle = DirectX::XMFLOAT3(0, 0, 0);
    scale = DirectX::XMFLOAT3(1, 1, 1);
    color = DirectX::XMFLOAT4(0, 0, 1, 1);
    //モデルの読み込み
    const char*filename = "Data/FBX/machine_girl/machine_girl.fbx";
    std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
    FbxLoader fbx_loader;
    fbx_loader.Load(filename, *model_data);
    std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
    obj = std::make_shared<Model>(model_resource);


    //obj_Render = std::make_shared<ModelRenderer>(device);
    //マウスの座標取得
    GetCursorPos(&pt);

    shot = std::make_unique<Shot>(device);
    exist = false;
}


void Player::Update() {
    Move();
    switch (Type) {
    case TYPE::SType:
        SetColor(DirectX::XMFLOAT4(0, 0, 1, 1));
        break;
    case TYPE::NType:
        SetColor(DirectX::XMFLOAT4(1, 0, 0, 1));
        break;
    }
    Move();
    //ワールド変換行列の初期化
    DirectX::XMMATRIX s, r, t;
    //拡大行列作成
    s = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
    //回転行列作成
    r = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);
    //移動行列作成
    t = DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);
    //行列合成と変換
    DirectX::XMStoreFloat4x4(&world, s * r * t);
    DirectX::XMMATRIX WORLD = DirectX::XMLoadFloat4x4(&world);
    obj->CalculateLocalTransform();
    obj->CalculateWorldTransform(WORLD);
}

void Player::Move() {
    const float speed = 0.2f;
    if (GetAsyncKeyState('W') < 0)
    {
        pos.z += speed;
    }
    if (GetAsyncKeyState('S') < 0)
    {
        pos.z -= speed;
    }
    //左右移動
    if (GetAsyncKeyState('A') < 0) {
        pos.x += speed;
    }
    if (GetAsyncKeyState('D') < 0) {
        pos.x -= speed;
    }
    if (GetAsyncKeyState('J') < 0) {
        angle.y += 0.05;
    }
    if (GetAsyncKeyState('L') < 0) {
        angle.y -= 0.05;
    }
    
    

}
//void Player::Render(ID3D11DeviceContext* context, 
//    float elapsed_time, const DirectX::XMFLOAT4X4 &view,
//    const DirectX::XMFLOAT4&light_direction)
//{
//    switch (Type) {
//    case TYPE::SType:
//        color = DirectX::XMFLOAT4(0, 0, 1, 1);
//        break;
//    case TYPE::NType:
//        color = DirectX::XMFLOAT4(1, 0, 0, 1);
//        break;
//    }
//    //ワールド行列を作成
//    DirectX::XMMATRIX W;
//    {
//
//        DirectX::XMMATRIX s, r, t;
//        s = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);//スケール行列
//        r = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);//回転行列
//        t = DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);//移動行列
//
//        W = s*r*t;
//    }
//
//  
//    obj->CalculateLocalTransform();
//    obj->CalculateWorldTransform(W);
//
//    obj_Render->Begin(context, view, light_direction);
//    obj_Render->Render(context, *obj, color);
//    if(shot->exist)
//    shot->Render(context, elapsed_time, view, light_direction, color);
//    obj_Render->End(context);
//};
