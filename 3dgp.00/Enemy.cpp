#include "Enemy.h"
#include "fbx_load.h"
#include "Collsion.h"
//----------------------------------
//  class:C_Enemy
//----------------------------------
C_Enemy::C_Enemy()
{
#if 0
    //パラメータ（仮）※後で修正する
    //speed = DirectX::XMFLOAT2(0, 0);
    pos = DirectX::XMFLOAT3(10, 0, 10);
    angle = DirectX::XMFLOAT3(0, 0, 0);
    scale = DirectX::XMFLOAT3(3, 3, 3);
    color = ColorList[EnemyType::Blue];

    //モデルの読み込み
    //const char*filename = ModelName[ModelNo];
    const char*filename = "Data//FBX//ganbo//ganbo.fbx";
    std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
    FbxLoader fbx_loader;
    fbx_loader.Load(filename, *model_data);
    std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
    obj = std::make_shared<Model>(model_resource);

    obj_Render = std::make_shared<ModelRenderer>(device);

    exist = false;
#endif
};
void C_Enemy::Init(ID3D11Device*device, std::shared_ptr<ModelResource>& resource)
{
#if 1


    scale = DirectX::XMFLOAT3(3, 3, 3);

    obj = std::make_shared<Model>(resource);

    obj_Render = std::make_shared<ModelRenderer>(device);

    exist = false;

    mover = nullptr;

    HitVec = { 0,0,0 };

    speed = 5.0;

    RefrectTime = 1.0;
    Hit = false;

    SetType(EnemyType::NORMAL);
#endif
};

void C_Enemy::Init(ID3D11Device*device)
{
    //パラメータ（仮）※後で修正する
    //speed = DirectX::XMFLOAT2(0, 0);
    pos = DirectX::XMFLOAT3(10, 0, 10);
    angle = DirectX::XMFLOAT3(0, 0, 0);
    scale = DirectX::XMFLOAT3(3, 3, 3);
    color = ColorList[EnemyType::Blue];

    //モデルの読み込み
    //const char*filename = ModelName[ModelNo];
    const char*filename = "Data//FBX//ganbo//ganbo.fbx";
    std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
    FbxLoader fbx_loader;
    fbx_loader.Load(filename, *model_data);
    std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
    obj = std::make_shared<Model>(model_resource);

    obj_Render = std::make_shared<ModelRenderer>(device);

    exist = false;
};
void C_Enemy::Update(float elapsedTime, XMFLOAT3 pos)
{
    if (Hit)
    {
        RefrectTime -= elapsedTime;
        if (RefrectTime > 0)
        {
            if (HitVec.x != 0 || HitVec.y != 0 || HitVec.z != 0)
            {
                this->pos.x += HitVec.x * 8 * elapsedTime;
                this->pos.y += HitVec.y * 8 * elapsedTime;
                this->pos.z += HitVec.z * 8 * elapsedTime;
            }
        }
        if (RefrectTime <= 0)
        {
            Hit = false;
            RefrectTime = 1.0;
            HitVec = XMFLOAT3(0, 0, 0);
        }
    };


#if 1
    //エネミーのスピード
    static float speed = 5.0f;
    //エネミーからプレイヤーへの向きをとる
    XMVECTOR start = XMVectorSet(this->pos.x, this->pos.y, this->pos.z, 0.0f);
    XMVECTOR end = XMVectorSet(pos.x, pos.y, pos.z, 0.0f);
    XMVECTOR vec = XMVectorSubtract(end, start);
    XMVECTOR normal = XMVector3Normalize(vec);//正規化

    XMVECTOR velocity = normal*speed;
    XMVECTOR displacement = velocity*elapsedTime;

    XMFLOAT3 floatdisp;
    XMStoreFloat3(&floatdisp, displacement);

    //角度計算
    XMFLOAT2 dist = { pos.x - this->pos.x,pos.z - this->pos.z };
    angle.y = atan2(dist.x, dist.y);
    //angle.y += 1.5*elapsedTime;


    this->pos.x += floatdisp.x;
    this->pos.y += floatdisp.y;
    this->pos.z += floatdisp.z;
#endif
    //speed = elapsedTime * 5;


  


    //ワールド行列を作成
    XMMATRIX W;
    {

        XMMATRIX s, r, t;
        s = XMMatrixScaling(scale.x, scale.y, scale.z);//スケール行列
        r = XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);//回転行列
        t = XMMatrixTranslation(this->pos.x, this->pos.y, this->pos.z);//移動行列

        W = s*r*t;
    }

    obj->CalculateLocalTransform();
    obj->CalculateWorldTransform(W);
};

void C_Enemy::Render(ID3D11DeviceContext* context, float elapsed_time, const XMFLOAT4X4 &view, const DirectX::XMFLOAT4&light_direction)
{

    obj_Render->Begin(context, view, light_direction);

    obj_Render->Render(context, *obj, color);

    obj_Render->End(context);

};

void C_Enemy::add(XMFLOAT3 pos, float angle)
{
    this->pos = pos;

    //this->angle = XMFLOAT3(0, 0, 0);
    //this->angle.y = angle;

    this->exist = true;


};

void C_Enemy::Destroy()
{
    exist = false;
    Hit = false;
}

//----------------------------------
//  class:C_EnemyManager
//----------------------------------
void C_EnemyManager::Init(ID3D11Device*device)
{
    //モデルの読み込み
    //const char*filename = ModelName[ModelNo];
    const char*filename = "Data//FBX//ganbo//ganbo.fbx";
    std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
    FbxLoader fbx_loader;
    fbx_loader.Load(filename, *model_data);
    model_resource = std::make_shared<ModelResource>(device, std::move(model_data));


    //listのサイズ確保
    //list.resize(100);

    for (int i = 0; i < 100; i++)
    {
        C_Enemy *it = &list[i];



        it->Init(device, model_resource);

        //if (i == 0)it->SetType(EnemyType::Blue);
        //if (i == 1)it->SetType(EnemyType::Red);
    };

    add(XMFLOAT3(-20, 0, 7));

    add(XMFLOAT3(20, 0, 10));

    //windowsize構造体の設定
    window.MaxX = 20;
    window.MinX = -20;
    window.MaxZ = 20;
    window.MinZ = -20;

    //サイズの設定
    size = 3.2;
    magnet = 6.0;
};

void C_EnemyManager::Update(float elapsedTime, XMFLOAT3 pos)
{
#if 1
    //時間経過で自動生成
    static float WaitTime = 5;
    WaitTime -= elapsedTime;
    if (WaitTime < 0)
    {
        WaitTime = 5;
        //add(XMFLOAT3(10, 0, -15));
        switch (rand()%4)
        {
        case SpawnType::Top:
            add(XMFLOAT3(rand() % 80 - 40, 0, 30));
            break;
        case SpawnType::Down:
            add(XMFLOAT3(rand() % 80 - 40, 0, -30));
            break;
        case SpawnType::Right:
            add(XMFLOAT3(-50, 0, rand() % 60 - 30));
            break;
        case SpawnType::Left:
            add(XMFLOAT3(50, 0, rand() % 60 - 30));
            break;
        }
    }

#endif

    //磁力の判定
    for (int i = 0; i < 100; i++)
    {
        C_Enemy*it = &list[i];
        if (it->isAlive() == false)continue;//存在したければcontinue
        for (int j = 0; j < 100; j++)
        {
            C_Enemy*HitModel = &list[j];
            if (i == j)continue;//自分自身とは判定を行わない
            if (HitModel->isAlive() == false)continue;//存在したければcontinue

            //EnemyType判定　（反発するか引き合うか）
            HitType hit = GetHitType(it->GetType(), HitModel->GetType());

            if (Collision::HitCollision(it->GetPos(), magnet, HitModel->GetPos(), magnet))
            {
                //当たった時の処理
                HitMove(it, HitModel, hit);
            };

        }
    };

    //オブジェクト同士の判定
    for (int i = 0; i < 100; i++)
    {
        C_Enemy*it = &list[i];
        if (it->isAlive() == false)continue;//存在しなければcontinue
        for (int j = 0; j < 100; j++)
        {
            C_Enemy*HitModel = &list[j];
            if (i == j)continue;//自分自身とは判定を行わない
            if (HitModel->isAlive() == false)continue;//存在しなければcontinue

            //EnemyType判定　（反発するか引き合うか）
            HitType hit = GetHitType(it->GetType(), HitModel->GetType());

            if (hit == HitType::Inverse)
            {
                if (Collision::HitCollision(it->GetPos(), size, HitModel->GetPos(), size))
                {
                    it->Destroy();
                    HitModel->Destroy();
                }
            };
        }
    };

    for (int i = 0; i < 100; i++)
    {
        C_Enemy *it = &list[i];
        if (it->isAlive())
        {
            it->Update(elapsedTime, pos);
        }
    };


};

void C_EnemyManager::Render(ID3D11DeviceContext* context, float elapsed_time, const XMFLOAT4X4 &view, const DirectX::XMFLOAT4&light_direction)
{
    for (int i = 0; i < 100; i++)
    {
        C_Enemy *it = &list[i];

        if (it->isAlive())
        {
            it->Render(context, elapsed_time, view, light_direction);
        }
    }

};

void C_EnemyManager::add(XMFLOAT3 pos)
{
    for (int i = 0; i < 100; i++)
    {
        C_Enemy *it = &list[i];

        if (it->isAlive() != true)
        {
            //（仮）あとで修正する
            it->add(pos, 0.5);

            break;
        };

    };
};

HitType C_EnemyManager::GetHitType(EnemyType Type1, EnemyType Type2)
{
    if (Type1 == EnemyType::NORMAL || Type2 == EnemyType::NORMAL)//どちらかのEnemyTypeがNormalだった場合
    {
        return HitType::Normal;
    };
    if (Type1 != Type2)//それぞれが違うタイプならば(SとN,NとS)
    {
        return HitType::Inverse;
    };

    if (Type1 == Type2)//それぞれが同じタイプならば(NとN,SとS)
    {
        return HitType::Same;
    };
};

void C_EnemyManager::HitMove(C_Enemy*My, C_Enemy*HitModel, HitType hit)
{
    My->SetHit(true);
    HitModel->SetHit(true);
    //MyからHitModelの向きのベクトル
    XMVECTOR start1 = XMVectorSet(My->GetPos().x, My->GetPos().y, My->GetPos().z, 1.0f);
    XMVECTOR end1 = XMVectorSet(HitModel->GetPos().x, HitModel->GetPos().y, HitModel->GetPos().z, 1.0f);
    XMVECTOR vec1 = XMVectorSubtract(end1, start1);
    XMVECTOR MyHit = XMVector3Normalize(vec1);//正規化

    //HitModelからMyの向きのベクトル
    XMVECTOR start2 = XMVectorSet(HitModel->GetPos().x, HitModel->GetPos().y, HitModel->GetPos().z, 1.0f);
    XMVECTOR end2 = XMVectorSet(My->GetPos().x, My->GetPos().y, My->GetPos().z, 1.0f);
    XMVECTOR vec2 = XMVectorSubtract(end2, start2);
    XMVECTOR HitMy = XMVector3Normalize(vec2);//正規化

    XMFLOAT3 myhit, hitmy;

    XMStoreFloat3(&myhit, MyHit);
    XMStoreFloat3(&hitmy, HitMy);

    switch (hit)
    {
    case HitType::Same://反発
        My->SetHitVec(hitmy);
        HitModel->SetHitVec(myhit);

        break;
    case HitType::Inverse://引き合う
        My->SetHitVec(myhit);
        HitModel->SetHitVec(hitmy);
        break;
    default:
        break;
    }



};


//-------------------------------------------------
//  Getter
//-------------------------------------------------
