#pragma once
#include "scene.h"
#include"sprite.h"
#include "Enemy.h"
#include "sound.h"
#include "Player.h"
#include "Score.h"
#include "Collsion.h"
#include <thread>
#include <mutex>
class Scene_Title :public Scene
{
private:
    std::shared_ptr<Model>					model;
    std::shared_ptr<ModelRenderer>			model_renderer;
    std::unique_ptr<C_Enemy> enemy;
    std::unique_ptr<Player> player;
    std::unique_ptr<Sound> Title_s;
    std::unique_ptr<Sprite> T_sprite;
    std::unique_ptr<Sprite> Push_Enter;
    bool a;
    int Counter;
public:
    std::unique_ptr<std::thread>loading_thread;
    std::mutex loading_mutex;



    Scene_Title(ID3D11Device*device);
    int Update(float elapsed_time);
    void Render(float elapsed_time, ID3D11DeviceContext* devicecontext);
    ~Scene_Title();
   
};