#include "Ranking.h"

bool Ranking::Initialize(ID3D11Device *Device)
{

    sprites = std::make_unique<Sprite>(Device, L"Data/image/num_font.png");  
    Rank_read();

    for (int i = 0; i < 5; i++)
    {
        if (rank[i] < nowtime)
        {

            rank[0] = nowtime;
            break;
        }
    }

    quick_sort(rank, left, 4);
    Rank_write();
    digit();

    return true;
}

void Ranking::Rank_read()
{
    FILE *fp;
    //読み込み
    std::string FileAD = { "Data/file1/ranking" };
    FileAD += ".txt";
    if (fopen_s(&fp, FileAD.data(), "rb") == 0)
    {
        fscanf_s(fp, "%d,%d,%d,%d,%d", &rank[0], &rank[1], &rank[2], &rank[3], &rank[4]);
        fclose(fp);
        quick_sort(rank, 0, 4);
    }
}


void Ranking::Rank_write()
{

    FILE *fp;
    //書き込み
    std::string FileAD = { "Data/file1/ranking" };
    FileAD += ".txt";
    if (fopen_s(&fp, FileAD.data(), "w") == 0) {
        for (int j = 0; j < 5; j++)
        {
            fprintf_s(fp, "%d,", rank[j]);
        }
    }
    fclose(fp);

}
int  Ranking::Update(float elapsdtime)
{


  return 0;
}

void Ranking::digit()
{
    //桁調べる　
    for (int i = 0; i < 5; i++)
    {
        digit_Score[i] = rank[i];
        while (digit_Score[i] != 0)
        {
            digit_Score[i] = digit_Score[i] / 10;
            ++digit_[i];
        }

    }


    //桁をバラバラにする
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < digit_[i]; j++)
        {
            _digit[i][j] = (rank[i] % 10);
            rank[i] /= 10;
            ++add;
        }
    }
}


void Ranking::Render(ID3D11DeviceContext* immediate_context, float elapsed_time)
{


    x = 950;
    //y = 750;
    size = 100;


    float  posy[5] = { 780.0f,660.0f,540.0f,410.0f,270.0f };
    float koron_y[5] = { 780.0f,660.0f,540.0f,410.0f,270.0f };//:の表示（Y軸を変更）



    for (int i = 0; i < 5; i++)
    {

        for (int j = 0; j < 4; j++)
        {
            sprites->render(immediate_context, x - (80.0f * j), posy[i], size, size, static_cast<float>(96 * _digit[i][j]), 0.0f, 96.0f, 96.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f);
            //sprites->render(immediate_context, x+100, y, 96, 96, 96 * (rank[i]%10), 0, 96, 96, 0, 1, 1, 1, 1);

        }
    }

    //：表示
    //for (int j = 0; j < 5; j++)
    //{
    //    sprites->render(immediate_context, 835.0f, koron_y[j], size, size, 96.0f * 10.0f, 0.0f, 96.0f, 96.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f);

    //}
    //ｍｙスコアの：標示
    //sprites->render(immediate_context, 210.0f, 420.0f, 96.0f, 96.0f, static_cast<float>(96 * 10), 0.0f, 96.0f, 96.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f);


    //if (hightFlag&&timer >= 7)
    //{
    //    hight_S->render(immediate_context, pos_x, pos_y, sizer_x, sizer_y, 0.0f, 0.0f, 768.0f, 96.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f);
    //    Line_A->render(immediate_context, 0.f, 0.f, 1980.f, 1080.f, x_A*anim, 0, x_A, y_A, 0.f, 1.f, 1.f, 1.f, 1.f);
    //}
}

void Ranking::swap(int *x, int *y)
{
    int temp;    // 値を一時保存する変数

    temp = *x;
    *x = *y;
    *y = temp;
}

int Ranking::partition(int array[], int left, int right)
{
    int i, j, pivot;
    i = left;
    j = right + 1;
    pivot = left;   // 先頭要素をpivotとする

    do {
        do { i++; } while (array[i] < array[pivot]);
        do { j--; } while (array[pivot] < array[j]);
        // pivotより小さいものを左へ、大きいものを右へ
        if (i < j) { swap(&array[i], &array[j]); }
    } while (i < j);

    swap(&array[pivot], &array[j]);   //pivotを更新

    return j;
}
void Ranking::quick_sort(int array[], int left, int right)
{
    int pivot;

    if (left < right) {
        pivot = partition(array, left, right);
        quick_sort(array, left, pivot - 1);   // pivotを境に再帰的にクイックソート
        quick_sort(array, pivot + 1, right);
    }
}
