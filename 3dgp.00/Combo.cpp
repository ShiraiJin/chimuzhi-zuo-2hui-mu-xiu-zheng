#include "Combo.h"
//a
void Combo::Init(ID3D11Device * Device)
{
    Combo_Sprite = std::make_unique<Sprite>(Device, L"Data/image/num_font.png");
    Combo_time_Sprite[0] = std::make_unique<Sprite>(Device, L"Data/image/combo_count_gauge.png");
    Combo_time_Sprite[1] = std::make_unique<Sprite>(Device, L"Data/image/combo_count_gauge_frame.png");
    Score_Sprite = std::make_unique<Sprite>(Device, L"Data/image/num_font.png");

    time_max = 10.0f;
    timer = time_max;
    combo = 0;
    com_ca = 0.0f;
    score_line = 5;
    score_max = 200.0f;
    upper_score_limit = 99999;

    ////----コンボカウント用----////
    combo_pos = DirectX::XMFLOAT2(500.0f, 20.0f);       //位置
    combo_size = DirectX::XMFLOAT2(96.0f, 96.0f);       //サイズ
    ////-----コンボタイム用-----////
    time_pos = DirectX::XMFLOAT2(450.0f, 120.0f);       //位置
    time_size = DirectX::XMFLOAT2(525.0f, 84.0f);       //サイズ
    ////--------スコア用--------////
    score_pos = DirectX::XMFLOAT2(1800.0f, 120.0f);     // 位置
    score_size = DirectX::XMFLOAT2(96.0f, 96.0f);       // サイズ
}

void Combo::Update(float elapsed_time, bool* combo_flag)
{
    if (*combo_flag == true)
    {
        if (combo >= combo_max)
        {
            combo = combo_max;
            com_ca = 30.0f;
        }
        else 
        {
            combo++;
            com_ca += 1.0f;
        }
        timer = time_max;
        Score(com_ca);
        this->combo_flag = true;
    }
    if (this->combo_flag)
    {
        combo_decomposition(combo);
        if (timer <= 0.0f) this->combo_flag = combo_time_flag = false;
        else timer -= elapsed_time;
    }
    else if (!combo_time_flag)
    {
        combo = 0;
    }

}

void Combo::Render(ID3D11DeviceContext * immediate_context, float elapsed_time)
{
    score_decomposition(high_score);
    for (int i = 0; i < score_line; i++)
    {
        Combo_Sprite->render(immediate_context,
            score_pos.x - score_size.x * static_cast<float>(i), score_pos.y,
            score_size.x, score_size.y,
            96.0f * static_cast<float>(score_row_decomposition[i]), 0.0f,
            96.0f, 96.0f,
            0.0f, 1.0f, 1.0f, 1.0f, 1.0f);
    }
    if (combo_flag) {
        for (int i = 0; i < 2; i++)
        {
            Combo_Sprite->render(immediate_context,
                combo_pos.x - combo_size.x * static_cast<float>(i), combo_pos.y,
                combo_size.x, combo_size.y,
                                 96.0f * static_cast<float>(combo_row_decomposition[i]), 0.0f,
                                 96.0f, 96.0f,
                                 0.0f, 1.0f, 1.0f, 1.0f, 1.0f);
        }
        //ゲージ
        Combo_time_Sprite[0]->render(immediate_context, 
                                     time_pos.x + 8.5f, time_pos.y + 6.5f,
                                     (timer / time_max) * (time_size.x - 17.0f), time_size.y - 13.0f,
                                     0.0f, 0.0f,
                                     525.0f - 17.0f, 84.0f - 13.0f,
                                     0.0f, 1.0f, 1.0f, 1.0f, 1.0f);
        //ゲージの枠
        Combo_time_Sprite[1]->render(immediate_context, time_pos.x, time_pos.y, time_size.x, time_size.y, 0.0f, 0.0f, 525.0f, 84.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f);
    }
}

void Combo::Score(float com_ca)
{
    score = score_max * (static_cast<float>(com_ca) / 10);
    if (high_score >= upper_score_limit) high_score = upper_score_limit;
    else high_score += static_cast<int>(score);
}

void Combo::combo_decomposition(int combo)
{
    for (int i = 0; i < 2; i++)
    {
        combo_row_decomposition[i] = (combo % 10);
        combo /= 10;
    }
}

void Combo::score_decomposition(int high_score)
{
    for (int i = 0; i < score_line; i++)
    {
        score_row_decomposition[i] = (high_score % 10);
        high_score /= 10;
    }
}

void Combo::score_division()
{
    //敵に当たったらスコア減らす
    high_score -= 500;
}

const int Combo::Get_High_Score() const
{
    return high_score;
}
