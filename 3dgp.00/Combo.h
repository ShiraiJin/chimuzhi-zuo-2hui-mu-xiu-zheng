#pragma once
#include <memory>
#include <vector>
#include"sprite.h"
//a
class Combo
{
private:
    const static int combo_max = 30;    // 最大コンボ数
    float time_max;                     // 最大コンボタイム
    float timer;                        // コンボタイム
    int combo;                          // コンボ
    float com_ca;                       // 計算用コンボint combo_calculation
    int score_line;                     // スコアの行
    int high_score;                     // 合計スコア
    int upper_score_limit;              // スコア上限
    float score_max;                    // 合成用スコア
    float score;                        // スコア
    ////----コンボカウント用----////
    DirectX::XMFLOAT2 combo_pos;        // 位置
    DirectX::XMFLOAT2 combo_size;       // サイズ
    ////-----コンボタイム用-----////
    DirectX::XMFLOAT2 time_pos;         // 位置
    DirectX::XMFLOAT2 time_size;        // サイズ
    ////--------スコア用--------////
    DirectX::XMFLOAT2 score_pos;        // 位置
    DirectX::XMFLOAT2 score_size;       // サイズ
    bool combo_flag = false;            // コンボフラグ
    bool combo_time_flag = false;       // コンボタイムフラグ
public:

	std::unique_ptr<Sprite> Combo_Sprite;
    std::unique_ptr<Sprite> Combo_time_Sprite[2];
    std::unique_ptr<Sprite> Score_Sprite;

    int combo_row_decomposition[100];
    int score_row_decomposition[100];

	void Init(ID3D11Device *Device);
	void Update(float elapsed_time, bool* combo_flag);
	void Render(ID3D11DeviceContext* immediate_context, float elapsed_time);
    void Score(float com_ca);
    void combo_decomposition(int combo);
    void score_decomposition(int high_score);
    void score_division();

    const int Get_High_Score() const;

	static Combo *getinctanse()
	{
		static Combo instance;
		return &instance;
	}
};

#define	pComboManager Combo::getinctanse()